#include <iostream>
#include <list>
#include <fstream>


using namespace std;


class Simpleks{

	public:
		double  A[3][5];
		double  b[3];
		int		Xb[3];
		int		Cb[3];
		double  Z[5];
		double  C_z[5];
		int CXmax[5];

		Simpleks(char*);
		void Wyswietl(void);
		void Obliczanie(void);
		void Wynik(void);

	private:
		void Czytaj(char*);
		int Max(double[]);
		int Min(int);
		

};


Simpleks::Simpleks(char *nazwa)
{

	for (int i = 0; i < 5; i++)
	{
		A[0][i] = 0;
		A[1][i] = 0;
		A[2][i] = 0;
		Z[i] = 0;
		C_z[i] = 0;
	}


	for (int i = 0; i < 3; i++)
	{
		b[i] = 0;
		Xb[i] = i+3;
		Cb[i] = 0;
	}


	Czytaj(nazwa);



}



void Simpleks::Czytaj(char *nazwa)
{
     fstream plik(nazwa);

	 if (!plik.good())
	 {
		 cout << "Blad odczytu z pliku" << endl;
		 return;
	 }

	 for (int j = 0; j < 5; j++)
		 plik >> CXmax[j];					//Wczytywanie funkcji celu

	 for (int i = 0; i < 3; i++)
	 {
		 for (int j = 0; j < 5; j++)
			 plik >> A[i][j];				//Macierz ograniczen

		 plik >> b[i];
	 }



}

int Simpleks::Max(double tab[])								//Funkcja zwracajaca indeks najwiekszego elementu w tablicy
{
	//int dlugosc = sizeof(tab) / sizeof(int);				// Ilosc elementow tablicy
	
	//cout << dlugosc;
	
	int tmp=0;												//Zmienna pomocnicza
	
	int index = -1;

	for (int i = 0; i<5;i++)
		
	if (tab[i] > tmp)
	{
		tmp = tab[i];
		index = i;
	}

	return index;

}

int Simpleks::Min(int baza)								//Funkcja zwracajaca indeks najwiekszego elementu w tablicy
{														//Kryterium wyjscia
	
	int tmp = 99999;												//Zmienna pomocnicza

	int index;

	for (int i = 0; i < 3; i++)
		if (A[i][baza] != 0)
			if (tmp >(b[i] / A[i][baza]))
			{
				tmp = (b[i] / A[i][baza]);
				index = i;
			}


	return index;

}


void Simpleks::Wyswietl(void)
{
	for (int i = 0; i < 3; i++)
	{
		cout << endl;
		for (int j = 0; j < 5; j++)
			cout << " " << A[i][j];
	}

	cout << endl << endl;

	for (int i = 0; i < 5; i++)
		cout << " " << Z[i];

	cout << endl << endl;

	for (int i = 0; i < 5; i++)
		cout << " " << C_z[i];

	cout << endl << endl;

	for (int i = 0; i < 3; i++)
		cout << " " << Cb[i];


	cout << endl << endl;

	for (int i = 0; i < 3; i++)
		cout << " " << Xb[i];

}


void Simpleks::Obliczanie(void)
{

	int baza;

	int tmp=0;

	double suma=0;

	double dzielenie;

	double mnozenie;

	for (int i = 0; i < 5; i++)
	{
		suma = 0;

		for (int j = 0; j < 3; j++)
			suma = suma + Cb[j] * A[j][i];

		Z[i] = suma;
	}

	for (int i = 0; i < 5; i++)
		C_z[i] = CXmax[i] - Z[i];



	while (true)
	{

		baza = Max(C_z);


		if (baza < 0)
			break;


		tmp = Min(baza);

		Xb[tmp] = baza + 1;

		Cb[tmp] = CXmax[baza];

		dzielenie = A[tmp][baza];

		for (int i = 0; i < 5; i++)
			A[tmp][i] = A[tmp][i] / dzielenie;

		b[tmp] = b[tmp] / dzielenie;


		for (int i = 0; i < 3; i++)
		if (tmp != i)
		{
			mnozenie = A[i][baza];



			b[i] = b[i] + b[tmp] * (-mnozenie);

			for (int j = 0; j < 5; j++)
				A[i][j] = A[i][j] + A[tmp][j] * (-mnozenie);




		}

		for (int i = 0; i < 5; i++)
		{
			suma = 0;

			for (int j = 0; j < 3; j++)
				suma = suma + Cb[j] * A[j][i];

			Z[i] = suma;
		}

		for (int i = 0; i < 5; i++)
			C_z[i] = CXmax[i] - Z[i];



	}
		
			


	


			

		
				












}


void Simpleks::Wynik(void)
{
	int x1, x2;

	for (int i = 0; i < 3; i++)
	{

		if (Xb[i] == 1)
			x1 = b[i];
		if (Xb[i] == 2)
			x2 = b[i];
	}

	cout << "X1= " << x1 << "   " << "X2= " << x2 << endl;

	cout << "Funkcja celu wynosi:" << x1*CXmax[0] + x2*CXmax[1] << endl;


}




int main(void)
{
	Simpleks program("dane01.txt");
	
	//program.Wyswietl();

	program.Obliczanie();

	program.Wynik();

	//program.Wyswietl();

	system("pause");

	return 0;
}